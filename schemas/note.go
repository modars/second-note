package schemas

import (
	"crypto/sha256"
	"errors"
	"github.com/graphql-go/graphql"
	"strconv"
	"time"
)

type Note struct {
	ID        string    `json:"id"`
	Content   string    `json:"content"`
	Tags      []string  `json:"tags"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"UpdatedAt"`
}

var noteType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Note",
	Fields: graphql.Fields{
		"id":        &graphql.Field{Type: graphql.String},
		"content":   &graphql.Field{Type: graphql.String},
		"tags":      &graphql.Field{Type: graphql.NewList(graphql.String)},
		"createdAt": &graphql.Field{Type: graphql.DateTime},
		"updatedAt": &graphql.Field{Type: graphql.DateTime},
	},
})

func GetNoteByID(p graphql.ResolveParams) (interface{}, error) {
	return getNoteByID(""), nil
}

func CreateNote(params graphql.ResolveParams) (interface{}, error) {
	content := params.Args["content"].(string)
	if len(content) > 1<<(10*2) { // more than 1M characters
		return nil, errors.New("too big size of content")
	}

	now := time.Now()

	generatedId := sha256.Sum256([]byte(
		content + strconv.FormatInt(now.UnixNano(), 16),
	))

	product := Note{
		ID:    string(generatedId[:]),
		Content:  content,
		Tags:  params.Args["tags"].([]string),
		CreatedAt: now,
		UpdatedAt:now,
	}

	// todo save

	return product, nil
}

func getNoteByID(id string) *Note {
	now := time.Now()
	return &Note{
		ID:        "30a0e28b-bff8-4393-8a8e-26a7333658ff",
		Content:   "example content",
		Tags:      []string{"tagA", "tagB"},
		CreatedAt: now,
		UpdatedAt: now,
	}
}
