package snippets

import (
	"github.com/mitchellh/go-homedir"
	"gopkg.in/src-d/go-git.v4"
	"os"
	"path/filepath"
	"testing"
)

var baseDir, _ = homedir.Expand("~/second-note/")
var testDir = filepath.Join(baseDir, "tmp/test")

func TestGitInit(t *testing.T) {
	/*
		** Takes MORE than a few seconds! **
	 */
	t.Skip() // to do this test, **COMMENT** this line

	defer func() {
		err := os.RemoveAll(testDir)
		if err != nil {
			t.Error(err)
		}
	}()

	_, _ = git.PlainClone(filepath.Join(testDir, "init"), false, &git.CloneOptions{
		URL:      "https://github.com/src-d/go-git",
		Progress: os.Stdout,
	})
}
