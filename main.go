package main

import (
	"errors"
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/modars/second-note/schemas"
	"net/http"
)

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/graphql", Query)
	e.GET("/", hello)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}

// Handler
func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}

func Query(c echo.Context) error {
	result := graphql.Do(graphql.Params{
		Schema:        schemas.Schema,
		RequestString: c.QueryParam("query"),
	})
	if len(result.Errors) > 0 {
		fmt.Println(result.Errors[0]) //todo
		return errors.New("GraphQL ERROR")
	}
	return c.JSON(200, result)
}
