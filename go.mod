module gitlab.com/modars/second-note

require (
	github.com/graphql-go/graphql v0.7.7
	github.com/labstack/echo v0.0.0-20181123063703-c7eb8da9ec73
	github.com/mitchellh/go-homedir v1.0.0
	gopkg.in/src-d/go-git.v4 v4.8.1
)
