![icon](./icon.png)

# Second Note

a note app that quick, synced, encrypted and offline-covered

This repository is for a server.
A simple client will be implemented in `tests/utils`.

### Other repositories

*None yet*

Various versions of client will be developed. (macos, Android, iOS, etc) 

## TODO

### Features

 - [ ] Get new note via GraphQL
 - [ ] Save a note as a file that git-managed
 - [ ] Encrypt notes
 - [ ] Organize notes
   - [ ] Tags 
 - [ ] Export to a remote repository
 - [ ] Simple client
    - [ ] Save encryption key
 - [ ] Multi-upload (import)
 
 - [ ] Cache in Redis
 - [ ] Auto-sync
 - [ ] Select tags to show as default

### sub

 - [ ] input -> file
 - [ ] Encrypt files
 - [ ] Sync to GitLab (temporarily)
    - [ ] Find somewhere better than GitLab

## considering...

### Tools

- viper
- prometheus

### think

- conflict during sync
- blockchain?
- server-integrated client package
